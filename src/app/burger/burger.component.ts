import {Component, Input} from '@angular/core';
import {Ingredient} from "../shared/ingredient.module";
import {IngredientsDisplay} from "../shared/ingredient-display.module";

@Component({
  selector: 'app-burger',
  templateUrl: './burger.component.html',
  styleUrls: ['./burger.component.css']
})
export class BurgerComponent {
  @Input() ingredients!: Ingredient[];
  @Input() showIngredients!: IngredientsDisplay;

  meat: string[] = [];
  cheese: string[] = [];
  salad: string[] = [];
  bacon: string[] = [];

  makeMeatArray() {
    for (let i = 0; i < this.showIngredients.burgerIngredients[0].amount; i++) {
      if (this.meat.length < this.showIngredients.burgerIngredients[0].amount) {
        this.meat.push('ingredient');
      } else if (this.meat.length > this.showIngredients.burgerIngredients[0].amount) {
        this.meat.splice(i, 1);
      }
    }
    return this.meat;
  }

  makeCheeseArray() {
    for (let i = 0; i < this.showIngredients.burgerIngredients[1].amount; i++) {
      if (this.cheese.length < this.showIngredients.burgerIngredients[1].amount) {
        this.cheese.push('ingredient');
      } else if (this.cheese.length > this.showIngredients.burgerIngredients[1].amount) {
        this.cheese.splice(i, 1);
      }
    }
    return this.cheese;
  }

  makeSaladArray() {
    for (let i = 0; i < this.showIngredients.burgerIngredients[2].amount; i++) {
      if (this.salad.length < this.showIngredients.burgerIngredients[2].amount) {
        this.salad.push('ingredient');
      } else if (this.salad.length > this.showIngredients.burgerIngredients[2].amount) {
        this.salad.splice(i, 1);
      }
    }
    return this.salad;
  }

  makeBaconArray() {
    for (let i = 0; i < this.showIngredients.burgerIngredients[3].amount; i++) {
      if (this.bacon.length < this.showIngredients.burgerIngredients[3].amount) {
        this.bacon.push('ingredient');
      } else if (this.bacon.length > this.showIngredients.burgerIngredients[3].amount) {
        this.bacon.splice(i, 1);
      }
    }
    return this.bacon;
  }

  meatClass() {
    if (this.showIngredients.burgerIngredients[0].amount > 0) {
      return 'Meat';
    } else {
      return '';
    }
  }

  cheeseClass() {
    if (this.showIngredients.burgerIngredients[1].amount > 0) {
      return 'Cheese';
    } else {
      return '';
    }
  }

  saladClass() {
    if (this.showIngredients.burgerIngredients[2].amount > 0) {
      return 'Salad';
    } else {
      return '';
    }
  }

  baconClass() {
    if (this.showIngredients.burgerIngredients[3].amount > 0) {
      return 'Bacon';
    } else {
      return '';
    }
  }
}
