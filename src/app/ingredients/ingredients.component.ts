import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Ingredient} from "../shared/ingredient.module";
import {IngredientsDisplay} from "../shared/ingredient-display.module";

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent {
  @Input() ingredients!: Ingredient[];
  @Output() displayIngredients = new EventEmitter();
  showIngredients = new IngredientsDisplay;

  imageUrls = [
    'https://cdn4.iconfinder.com/data/icons/burger-6/500/bep146_29_burger_meat_cartoon_dog_retro_food_vintage-512.png',
    'https://media.istockphoto.com/vectors/cheese-slice-square-vector-id497079307?k=20&m=497079307&s=612x612&w=0&h=SEWmx21MeUU_87Sh2Jvih8_54H_URrhIdBcUuVKqyZ0=',
    'https://static.vecteezy.com/system/resources/previews/001/932/069/non_2x/fresh-lettuce-vegetable-healthy-icon-free-vector.jpg',
    'https://static.wikia.nocookie.net/gensin-impact/images/1/16/Item_Bacon.png'];

  countIngredients(i: number) {
    this.ingredients[i].amount++;
    this.showIngredients.burgerIngredients[i].display = true;
    this.showIngredients.burgerIngredients[i].amount = this.ingredients[i].amount;
    this.displayIngredients.emit(this.showIngredients);
  }

  reduceAmount(i: number) {
    if (this.ingredients[i].amount != 0) {
      if (this.ingredients[i].amount === 1) {
        this.showIngredients.burgerIngredients[i].display = false;
      }
      this.ingredients[i].amount--;
      this.showIngredients.burgerIngredients[i].amount = this.ingredients[i].amount;
    }
  }


}
