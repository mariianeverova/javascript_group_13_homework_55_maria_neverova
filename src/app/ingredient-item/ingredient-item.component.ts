import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Ingredient} from "../shared/ingredient.module";

@Component({
  selector: 'app-ingredient-item',
  templateUrl: './ingredient-item.component.html',
  styleUrls: ['./ingredient-item.component.css']
})
export class IngredientItemComponent {
  @Input() ingredient!: Ingredient;
  @Input() imageUrl = '';
  @Output() addAmount = new EventEmitter();
  @Output() reduceAmount = new EventEmitter();
  disabled = true;

  buttonDisable() {
    if (this.ingredient.amount > 0) {
      this.disabled = false;
    } else if (this.ingredient.amount === 0) {
      this.disabled = true;
    }
    return this.disabled;
  }

  onAddCount() {
    this.addAmount.emit();
    console.log('added');
  }

  onReduceAmount() {
    this.reduceAmount.emit();
    console.log('reduced');
  }
}
