export class IngredientsDisplay {
  burgerIngredients = [
    {name: 'Meat', display: false, amount: 0},
    {name: 'Cheese', display: false, amount: 0},
    {name: 'Salad', display: false, amount: 0},
    {name: 'Bacon', display: false, amount: 0}];
}
